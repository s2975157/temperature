package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Calculation {
    private static final double nineFifths = 9.0/5.0;

    public static double celsiusToFahrenheit(double celsius) {
        return (celsius * nineFifths) + 32;
    }
}
